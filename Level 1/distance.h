#ifndef DISTANCE_h
#define DISTANCE_h

#include <stdbool.h>

#define PI 3.14159265358979323846

//  Function prototypes
bool ValidityCheck(double arr[]);
bool SeparateString(char *latLongStr, double arr[]);
bool TakeInput(double latLongArr[]);
double Deg2Rad(double deg);
double Rad2Deg(double deg);
double Distance(double lat1, double lon1, double lat2, double lon2, char unit);

#endif