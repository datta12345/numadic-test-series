#include "distance.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
* @description Check if the converted values are valid latitude and longitude values
* @param array pointer which will hold lat value and long value
* @returns bool true if valid data 
*/
bool ValidityCheck(double arr[]){
    if (arr[0] < -90.0 || arr[0] > 90.0)
        return false;
    else if (arr[1] < -180.0 || arr[1] > 180.0)
        return false;
    else
        return true;
}

/*
* @description Check if the converted values are valid latitude and longitude values
* @param1 char * which holds the user entered data
* @param2 array pointer which will hold lat value and long value
* @returns bool true if string separated and converted succesfully
*/
bool SeparateString(char *latLongStr, double arr[]){
    int commaPos;
    int inputStrLen = strlen(latLongStr);
    char *comma = strchr(latLongStr, ',');
    if(comma == NULL) //if comma si not present
        return false;
    commaPos = (int)(comma - latLongStr); //get comma pos
    char latStr[commaPos+1];
    char longStr[(inputStrLen-commaPos)+1];
    char * unwantedChar;
    int i;
    strncpy(latStr,latLongStr,commaPos); //copy till comma pos
    arr[0] = strtod(latStr,&unwantedChar); 
    if(latStr == unwantedChar || *unwantedChar != '\0') // check if entire latStr is a number
        return false;
    i = commaPos+1;
    if(i>inputStrLen)
        return false;
    while(i<inputStrLen){
        longStr[i-(commaPos+1)] = latLongStr[i]; // copy string after comma into longStr
        i++;
    }
    arr[1] = strtod(longStr,&unwantedChar);
    if(longStr == unwantedChar || *unwantedChar != '\0')
        return false;
    return true;
}

/*
* @description Check if the converted values are valid latitude and longitude values
* @param array pointer which will hold lat value and long value
* @returns bool true if input from user is acceptable
*/
bool TakeInput(double latLongArr[]){
    char latLongStr[30];
    printf("Please enter lat,long values:");
    scanf("%s",latLongStr);
    printf("\n");
    if (!SeparateString(latLongStr,latLongArr))
        return false;
    if (ValidityCheck(latLongArr)) {
        return true;
    }
    return false;
}


/*
* @description calculate distance based on haversine formula
* @param1 double latitude value 1
* @param2 double longitude value 1
* @param3 double latitude value 2
* @param4 double longitude value 2
* @param5 char K: Kms , M: Miles N: Nautical Miles
* @returns double distance value
*/
double Distance(double lat1, double lon1, double lat2, double lon2, char unit){
    double theta, dist;
    theta = lon1 - lon2;
    dist = sin(Deg2Rad(lat1)) * sin(Deg2Rad(lat2)) + 
           cos(Deg2Rad(lat1)) * cos(Deg2Rad(lat2)) * cos(Deg2Rad(theta));
    dist = acos(dist);
    dist = Rad2Deg(dist);
    dist = dist * 60 * 1.1515;
    switch(unit) {
        case 'M':
            break;
        case 'K':
            dist = dist * 1.609344;
            break;
        case 'N':
            dist = dist * 0.8684;
        break;
    }
    return (dist);
}

/*
* This function converts decimal degrees to radians 
*/
double Deg2Rad(double deg) {
    return (deg * PI / 180);
}

/*
* This function converts radians to decimal degrees
*/
double Rad2Deg(double rad) {
    return (rad * 180 / PI);
}

