#include "distance.c"

#include <stdio.h>

int main(void){
    double lat1 ;
    double long1 ;
    double lat2 ;
    double long2 ;
    double latLongArr[2];
    printf("Hello, enter two lat,long pairs on promt\n");
    printf("Example: 15.4909,73.8278\n");
    while (!TakeInput(latLongArr))
        printf("Please enter Valid lat,long values!\n");
    lat1 = latLongArr[0];
    long1 = latLongArr[1];
    printf("First pair entered: lat=%f long=%f\n",  lat1, long1);
    while (!TakeInput(latLongArr))
        printf("Please enter Valid lat,long values!\n" );
    lat2 = latLongArr[0];
    long2 = latLongArr[1];
    printf("Second pair entered: lat=%f long=%f\n",  lat2, long2);
    double dist = Distance(lat1,long1,lat2,long2,'K');
    printf("Distance in Km is: %f\n", dist);
    return 0;
}   