#include "distance.c"

#include <stdio.h>

int main(void){
    double lat1 = 15.4909;
    double long1 =  73.8278;
    double lat2 = 19.0760;
    double long2 =  72.8777;
    double dist = Distance(lat1,long1,lat2,long2,'K');
    printf("Distance in Kms is: %f \n", dist) ;
    return 0;
}
