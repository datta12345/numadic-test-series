#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "MQTTClient.h"
#include <pthread.h>
#include <iostream>
#include <stdbool.h>
#include <unistd.h>
#include "file_handler.h"

#define ADDRESS     "tcp://139.59.63.74:23654"
#define CLIENTID    "Persistent_Client"
#define TOPIC       "MESSAGES"
#define QOS         1
#define TIMEOUT     10000L

MQTTClient client;
MQTTClient_message pubmsg = MQTTClient_message_initializer;
MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
MQTTClient_deliveryToken token;

#define MSG_DELAY 30 //msg generation interval

// A normal C function that is executed as a thread when its name
// is specified in pthread_create()
 
using namespace std; 

unsigned long msg_count = 0; //track msgs

bool isConnected = false; //store State of connection

/*
* @description Generate required msg
* @returns std::string message generated
*/
string GenerateMessage(){
	msg_count++;
	string msg = "message num:" ;
	string count =  to_string(msg_count);
	return msg + count;
}


/*
* @description Thread function to generate msgs every 30 seconds
* @returns std::string message generated
*/
void *MessageGenerator(void *vargp){
    while (true){
    	sleep(MSG_DELAY);
    	string message = GenerateMessage();
    	if(isConnected){
    	    cout << "current message = " << message << endl;
			pubmsg.payload = (void*)message.c_str();
			pubmsg.payloadlen = (message).length();
			MQTTClient_publishMessage(client, TOPIC, &pubmsg, &token);
    	}
    	else{
    	    cout << "storing message = " << message << endl;
    	    WriteMsgToFile(message);
    	}
    }
    return NULL;
}

/*
* @description fucntion for connection
* @returns bool True if connected to host succesfully
*/
bool Connect(){
	int rc;
	if (!MQTTClient_isConnected(client)){ //check if client is connected 
		rc = MQTTClient_connect(client, &conn_opts);
		if (rc == MQTTCLIENT_SUCCESS){
			isConnected = true;
		}
		else
			isConnected = false;
	}
    return isConnected;
}

int main(){
	string storedMsg;
	pthread_t tid;
	bool fileEnd = false;
	bool firstTime = true;
    MQTTClient_create(&client, ADDRESS, CLIENTID,
        MQTTCLIENT_PERSISTENCE_NONE, NULL);
    conn_opts.keepAliveInterval = 20;
    conn_opts.cleansession = 1;
	pubmsg.qos = QOS;
    pubmsg.retained = 0;
	int msgsInFile;
	while (true){
		if (Connect()){
			if(firstTime){
    	        cout << "Starting msg generation" << endl;
    	        pthread_create(&tid, NULL, MessageGenerator, NULL);
    	        firstTime = false;
    	    }	
			msgsInFile = countLine(); 
			if (msgsInFile != 0){ //check if lines exsit in log file
				int i = 0;
				while(i < msgsInFile){
					isConnected = false; //new messages generated will be logged first
					sleep(1);
					storedMsg = GetMessageFromFile(i) ; //read file line by line
					cout << storedMsg << endl;
					pubmsg.payload = (void*)storedMsg.c_str(); 
					pubmsg.payloadlen = (storedMsg).length();
					MQTTClient_publishMessage(client, TOPIC, &pubmsg, &token); 
					i++; // point to next line in file
					
				}
				isConnected = true; // after all logged messages are published disable logs
				DeleteFileContents(); // delete file after all lines are written
			}
		}
	}
	pthread_exit(&tid);
    MQTTClient_disconnect(client, 10000);
    MQTTClient_destroy(&client);
    return 0;
}