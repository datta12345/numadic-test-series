#Code Implementation#
* This code makes use of the pthreads library to generate msgs every 30 seconds
* It also use paho mqtt c Client for MQTT functionality 
* The server it connects to run a mosquitto broker (ip 139.59.63.74 on port 23654)
* To install the broker following guide was reffered 
** Link : http://www.xappsoftware.com/wordpress/2015/05/18/six-steps-to-install-mosquitto-1-4-2-with-websockets-on-debian-wheezy/comment-page-1/
* To make the broker secure with TCP and for generation of self signed Certificates following links were reffered
** Link 1: https://primalcortex.wordpress.com/2016/03/31/mqtt-mosquitto-broker-with-ssltls-transport-security/
** Link 2: https://primalcortex.wordpress.com/2016/11/08/mqtt-mosquitto-broker-client-authentication-and-client-certificates/
* For installing the C libraries required to make the client the following link was reffered
** Link : https://eclipse.org/paho/clients/c/
*** Documentation link : http://www.eclipse.org/paho/files/mqttdoc/Cclient/index.html

## Logical Flow ##
* Same as Level three
** Minor exceptions
*** Added functionality in file handler.h to read log file
*** Added functionality in main to make the file behave like a fifo

# NOTE #
* The security was tested using the above links and command line connect examples
* The same failed to connect using the Paho-mqtt-c client
* On further research it was realized that too many user had the same issue
** Java client works but exact same C client doesn't
** Tried multiple solutions for the same but none seemed to work.
** Certificate verification tested over command line
** certificates paths tested over command line
** protocol tcp returned the following error always in broker logs : SSL3_GET_RECORD:wrong version number 
** protocol ssl sent no pings to the broker in the logs file.
* Thus the code does not meet the requirements of the test.
