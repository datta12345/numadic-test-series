#ifndef FILE_HANDLER_h_
#define FILE_HANDLER_h_

#include <string>
#include <iostream>
#include <fstream>
#include <stdbool.h>

using namespace std;

/*
* @description function to delete log file contents
* when trunc is called for an output file it is deleted if it exists 
* also new file is created
* @returns void
*/
void DeleteFileContents(){
	
    ofstream messagesFile ("messages.txt", ios::out | ios::trunc ); 
    messagesFile.close();
}

/*
* @description function to write a single message to the file
* option app for an output file appends to the bottom of the file
* @returns void
*/
void WriteMsgToFile(string currentMsg){
	ofstream messagesFile ("messages.txt", ios::out | ios::app);
	if (messagesFile.is_open()) {
		messagesFile << currentMsg << "\n";
	}
	messagesFile.close();
}

/*
* @description function read a line from the file 
* @param int lineCount , number of the line to be read
* @returns string line read from file
*/
string GetMessageFromFile(int lineCount){
	string line = "Error in file";
	int currentLineCount = 0;
	ifstream messagesFile ("messages.txt");
  	if (messagesFile.is_open()){
    	while ( getline (messagesFile,line) ){
			if (currentLineCount == lineCount){
				messagesFile.close();
				return line;
			}
			currentLineCount++;
    	}
  	}
	messagesFile.close();
	return line;
}

/*
* @description function to get count of lines in a file
* @returns int number of lines in file
*/
int countLine(){
	string line;
	int lineCount = 0;
    ifstream messagesFile ("messages.txt");
    while(getline(messagesFile,line)) lineCount++;
    return lineCount;
}

#endif