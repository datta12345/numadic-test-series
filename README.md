# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Holds codes for Numadic Test Series
* Version 0.1.0

### How do I get set up? ###

* Clone the repo into your local system
* Build for your system using gcc
* For level 1 code use -lm as the math.h library is used in distance header
** Example: gcc distance_fixed -o fixed_dist_output -lm
* For level three tests use g++ -std=c++0x -pthread test_3.cpp 
** This is to compile with thread functions and also to_string functions requirement

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* email: datta12345@gmail.com
