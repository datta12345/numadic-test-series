#include "file_handler.c"

#include <stdio.h>
#include <stdlib.h>

int main(void){
   FILE *raw_data;
   printf("Starting to Process raw_dump.io\n");
   if ((raw_data = fopen("raw_dump.io","r")) == NULL){
       printf("Error! opening file");
       // Program exits if the file pointer returns NULL.
       exit(1);
   }
   printf("File open success\n");
   ProcessFile(raw_data);
   printf("Processing Complete\n");
}