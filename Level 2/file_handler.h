#ifndef FILE_HANDLER_h_
#define FILE_HANDLER_h_

#include <stdio.h>

#define LAT 0
#define LONGI 1

void ProcessFile(FILE* fp);
void ProcessLine(char *currLine);
void checkGPRMCAndDisplay(char *currLine);
void PrintVelocity(char *vel);
void PrintTime(char *time);
void PrintLatitude(char *lat);
void PrintLongitude(char *longi);

#endif