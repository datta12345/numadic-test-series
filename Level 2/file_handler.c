#include "file_handler.h"

#include <stdlib.h>
#include <string.h>


/*
* @description print velocity after decoding it in correct format
* @param1 char * which holds the GPRMC velocity value
* @returns void
*/
void PrintVelocity(char *vel){
	float speed ;
	int lenVel = strlen(vel);
	char actualVel[lenVel];
	int i;
	if(vel[0] == '0'){
		for ( i = 1; i < lenVel ; i++){
			actualVel[i-1]=vel[i];	
		}
	}
	else{
		for ( i = 0; i < lenVel ; i++){
			actualVel[i]=vel[i];	
		}
	}
	speed = atof(actualVel);
	printf("Velocity= %.2f Knots\n", speed);
}

/*
* @description covert GPRMC lat long values into degree
* Latitude format from GPRMC ddmm.mmmm d- deg m - minutes
* Longitutde format from GPRMC dddmm.mmmm 
* @param1 char * which holds the GPRMC lat / Longitude value
* @param2 type Lat / Longi LAT = 0 ; LONGI = 1
* @returns double converted value in degrees
*/
double convertValue(char *str, int type){
	double deg ;
	double min ;
	double returnVal;
	int i;
	if(type == LAT){
		deg = (str[0] - '0') * 10.0 + (str[1] - '0');
		min = (str[2] - '0')* 10.0 + (str[3] - '0');
		min += ((str[4]- '0')*1000.0 + (str[5]- '0')*100 + (str[6]- '0')*10 + (str[7]- '0'))/10000;
		min = min/60.0;
		returnVal = deg + min;
		return returnVal;
	}
	deg = (str[0] - '0') * 100.0 + (str[1] - '0')* 10 + (str[2]- '0');
	min = (str[3] - '0') * 10.0 + (str[4] - '0');
	min += ((str[5]- '0')*1000.0 + (str[6]- '0')*100 + (str[7]- '0')*10 + (str[8]- '0'))/10000;
	min = min/60.0;
	returnVal = deg + min;
	return returnVal;
}

/*
* @description print latitude after decoding it in correct format
* @param1 char * which holds the GPRMC latitude value
* @returns void
*/
void PrintLatitude(char *lat){
	double actualLat = convertValue(lat,LAT);
	printf("%f N, ", actualLat);
}

/*
* @description print longitude after decoding it in correct format
* @param1 char * which holds the GPRMC longitude value
* @returns void
*/
void PrintLongitude(char *longi){
	double actualLong = convertValue(longi,LONGI);
	printf("%f E ", actualLong);
}


/*
* @description print time after decoding it in correct format
* @param1 char * which holds the GPRMC time value
* Format HHMMSS
* @returns void
*/
void PrintTime(char *time){
	printf(" %c%c:%c%c:%c%c ", time[0],time[1],time[2],time[3],time[4],time[5]);
}

/*
* @description print date after decoding it in correct format
* @param1 char * which holds the GPRMC date value
* Format DDMMYY
* @returns void
*/
void PrintDateTime(char *time,char *date){
	printf("Timestamp: ");
	printf(" %c%c/%c%c/%c%c ", date[0],date[1],date[2],date[3],date[4],date[5]);
	PrintTime(time);
	printf("\n");
}

/*
* @description print latitude and longitude 
* @param1 char * which holds latitude value in degress
* @param2 char * which holds longitude value in degress
* @returns void
*/
void PrintLatLong(char* lat, char* longi){
	printf("Lat/Long: ");
	PrintLatitude(lat);
	PrintLongitude(longi);
	printf("\n");
}

/*
* @description process raw dump file for data extraction 
* @param1 FILE * which holds a pointer to opened file
* @returns void
*/
void ProcessFile(FILE *dataFile){
	char currLine[100]; // Max values a line doesnt exceed 100 
	while ( fgets ( currLine, sizeof currLine, dataFile ) != NULL ){ //Iterate over file line by line
      	ProcessLine(currLine);
    }
    fclose ( dataFile );
}

/*
* @description process lines in files to find GPRMC line
* @param1 char * which hold current linie extracted from raw data file
* @returns void
*/
void ProcessLine(char *currLine){
	checkGPRMCAndDisplay(currLine);
}

/*
* @description check if line is a GPRMC Line and display accordingly
* @param1 char * which hold current linie extracted from raw data file
* @returns void
*/
void checkGPRMCAndDisplay(char* currLine){
	int count = 0;
	char checkString[] = "$GPRMC";
	char *time;
	char *lat;
	char *pch;
	pch = strtok (currLine,","); // break string into parts on "," token
  	while (pch != NULL){
    	if(count == 0){ 
    		if(strcmp(pch,checkString) != 0) //here pch is $GPRMC check
    			break;
		}
    	if(count == 1){ //here pch is time
    		time = pch;
    	}
    	if(count == 3){ //here pch is lat
    		lat = pch;
    	}	
    	if(count == 5){ //here pch is longi
    		PrintLatLong(lat,pch); //print both 
    	}
    	if(count == 7){ //here pch is vel
    		PrintVelocity(pch);
    	}
    	if(count == 9){ //here pch is date
    		PrintDateTime(time, pch); //print both as timeStamp
    		printf("\n");
    	}
    	pch = strtok (NULL, ",");
    	count++;
  	}
}