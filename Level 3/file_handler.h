#ifndef FILE_HANDLER_h_
#define FILE_HANDLER_h_

#include <string>
#include <iostream>
#include <fstream>

using namespace std;

void DeleteFileContents(){
    ofstream messagesFile ("messages.txt", ios::out | ios::trunc);
    messagesFile.close();
}

void WriteMsgToFile(string currentMsg){
	ofstream messagesFile ("messages.txt", ios::out | ios::app);
	if (messagesFile.is_open()) {
		messagesFile << currentMsg << "\n";
	}
	messagesFile.close();
}

#endif