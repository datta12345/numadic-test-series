#Code Implementation#
* This code makes use of the pthreads library to generate msgs every 30 seconds
* A custom getch() function is written to get single char input from the console
* The above is done as conio.h is not supported directly in Linux 
* The messages that are generated when the system is not connected to the host are stored in messages.txt file
* The file is truncated when the system connection is re esatblished

## Logical Flow ##
* In the main Loop call Connect 
** Currently implemented to take the char input from terminal and connect or disconnect
*** Future implementation would be to add UART based AT commands to connect to a physical host
* If connect returns true for the first time start the thread to generate messages
** If not the first time that the system has connected delete file contents on re-connection
* The message generator thread will keep creating numbered messages every 30 seconds
** if the flag isConnected is true it will simply print the msg on the console
*** Future implementation it can send the msg to a physical device
** else if the flag isConnected is false it will write the message to a log file messages.txt