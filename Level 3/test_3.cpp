#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <iostream>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <termios.h>
#include "file_handler.h"

#define MSG_DELAY 5 //msg generation interval

// A normal C function that is executed as a thread when its name
// is specified in pthread_create()
 
using namespace std; 

unsigned long msg_count = 0; //track msgs

bool isConnected = false; //store State of connection

/*
* @description Generate required msg
* @returns std::string message generated
*/
string GenerateMessage(){
	msg_count++;
	string msg = "message num:" ;
	string count =  to_string(msg_count);
	return msg + count;
}


/*
* @description Thread function to generate msgs every 30 seconds
* @returns std::string message generated
*/
void *MessageGenerator(void *vargp)
{
    while (true){
    	sleep(MSG_DELAY);
    	string message = GenerateMessage();
    	if(isConnected){
    		cout << "current message = " << message << endl;
    	}
    	else{
    		cout << "storing message = " << message << endl;
    		WriteMsgToFile(message);
    	}

    }
    return NULL;
}

/*
* @description Psuedo fucntion for connection
* @returns bool True if connected to host succesfully
*/
bool Connect(char inputChar){
	cout << inputChar ;

    if (inputChar == 'D'){
    	if (isConnected)
    		cout << endl << "Disconnected from system" << endl;
    	else
    		cout << endl << "Already disconnected from system" << endl;
    	isConnected = false;
    }
    else if (inputChar == 'C'){
    	if (!isConnected)
    		cout << endl << "Connected to system" << endl;
    	else
    		cout << endl << "Already connected to system" << endl;
    	isConnected = true;
    }
    else
    	cout << endl << "Command not understood" << endl;
    return isConnected;
}

/*
* @description implements getch function similar to conio.h for windows
* @returns char typed in console by user
*/
char getch() {
        char buf = 0;
        struct termios old = {0};
        if (tcgetattr(0, &old) < 0)
                perror("tcsetattr()");
        old.c_lflag &= ~ICANON;
        old.c_lflag &= ~ECHO;
        old.c_cc[VMIN] = 1;
        old.c_cc[VTIME] = 0;
        if (tcsetattr(0, TCSANOW, &old) < 0)
                perror("tcsetattr ICANON");
        if (read(0, &buf, 1) < 0)
                perror ("read()");
        old.c_lflag |= ICANON;
        old.c_lflag |= ECHO;
        if (tcsetattr(0, TCSADRAIN, &old) < 0)
                perror ("tcsetattr ~ICANON");
        return (buf);
} 


int main()
{
	char inputChar;
	bool firstTime = true;
    pthread_t tid;
    cout << "Connect to server by pressing \"C\"" << endl;
    cout << "Disconnect from server by pressing \"D\"" << endl;
    while(true){
    	inputChar = getch();
    	if (Connect(inputChar)){
    		if(firstTime){
    			cout << "Starting msg generation" << endl;
    			pthread_create(&tid, NULL, MessageGenerator, NULL);
    			firstTime = false;
    		}
    		DeleteFileContents();
    	}
    }
    pthread_exit(&tid);
    cout << "After Thread" << endl;
    exit(0);
}